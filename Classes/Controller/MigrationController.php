<?php
namespace SBublies\Gridtocontainer\Controller;


use SBublies\Gridtocontainer\Domain\Repository\MigrationRepository;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use Psr\Http\Message\ResponseInterface;
/***
 *
 * This file is part of the "Gridtocontainer" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2021 Stefan Bublies <project@sbublies.de>
 *
 ***/

use TYPO3\CMS\Extbase\Annotation\Inject;

/**
 * MigrationController
 */
class MigrationController extends ActionController
{

		/**
		 * migrationRepository
		 *
		 * @var \SBublies\Gridtocontainer\Domain\Repository\MigrationRepository
		 * @Inject
		 */
		protected $migrationRepository = null;

        public function __construct(MigrationRepository $migrationRepository)
        {
            $this->migrationRepository = $migrationRepository;
        }

    /**
		 * action list
		 *
		 * @return void
		 */
		public function listAction(): ResponseInterface
		{
				$gridelementsElements = $this->migrationRepository->findGridelements();
				$this->view->assign('gridelementsElements', $gridelementsElements);
				return $this->htmlResponse();
		}

		/**
		 * action process
		 *
		 * @return void
		 */
		public function processAction(): ResponseInterface
		{
			// Form Data
			$arguments = $this->request->getArguments();
			$elementIds = $arguments['migration']['elements'];
			$contentElements = [];
			$gridelementsElements = [];
			foreach($elementIds as $id){
				if(empty($id)){
					continue;
				} else {
					$contentElements[$id] = $this->migrationRepository->findContentfromGridElements($id);
					$gridelementsElements[$id] = $this->migrationRepository->findById($id);
				}
			}
			$this->view->assignMultiple(
				array(
					"gridElements" => $gridelementsElements,
					"contentElements" => $contentElements,
					"arguments" => $arguments
				)
			);
			return $this->htmlResponse();
		}

		/**
		 * action migrategeneral
		 *
		 * @return void
		 */
		public function migrategeneralAction(): ResponseInterface
		{
			$gridelementsElements = $this->migrationRepository->findGridelementsCustom();
			$gridElementsArray=[];
			foreach ($gridelementsElements as $element) {
				$gridElementsArray[$element['tx_gridelements_backend_layout']] = $element;
			}

			$this->view->assignMultiple(
				array(
					"gridelementsElements" => $gridElementsArray,
					"contentElements" => $this->migrationRepository->findContent($gridElementsArray)
				)
			);
			return $this->htmlResponse();
		}

		/**
		 * action migrateprocess
		 *
		 * @return void
		 */
		public function migrateprocessAction()
		{
			// Form Data
			$arguments = $this->request->getArguments();
			$migrateAllElements = $this->migrationRepository->updateAllElements($arguments['migrategeneral']['elements']);
			$this->view->assignMultiple(
				array(
					"arguments" => $arguments,
					"migrateAllElements" => $migrateAllElements
				)
			);
		}

		/**
		 * action migrate
		 *
		 * @return void
		 */
		public function migrateAction(): ResponseInterface
		{
			// Form Data
			$arguments = $this->request->getArguments();

			$migrateContainerElements = $this->migrationRepository->updateGridElements($arguments['migration']['elements']);
			$migrateContentElements = $this->migrationRepository->updateContentElements($arguments['migration']['contentElements']);

			$this->view->assignMultiple(
				array(
					"arguments" => $arguments,
					"ContainerElementResult" => $migrateContainerElements,
					"ContentElementResult" => $migrateContentElements
				)
			);
			return $this->htmlResponse();
		}

		/**
		 * action analyse
		 *
		 * @return void
		 */
		public function analyseAction(): ResponseInterface
		{
			$gridelementsElements = $this->migrationRepository->findGridelementsCustom();
			$gridElementsArray=[];
			foreach ($gridelementsElements as $element) {
				$gridElementsArray[$element['tx_gridelements_backend_layout']] = $element;
			}
			$this->view->assignMultiple(
				array(
					"gridElements" => $gridElementsArray
				)
			);
			return $this->htmlResponse();
		}

		/**
		 * action overview
		 *
		 * @return void
		 */
		public function overviewAction(): ResponseInterface
		{
			//Overview
			return $this->htmlResponse();
		}

}
