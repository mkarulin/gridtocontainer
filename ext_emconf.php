<?php

/***************************************************************
 * Extension Manager/Repository config file for ext: "gridtocontainer"
 *
 *
 * Manual updates:
 * Only the data in the array - anything else is removed by next write.
 * "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = [
    'title' => 'Migration Gridelements to Container',
    'description' => 'EXT:gridtocontainer is a small migration extension with backend module for those who want to switch from EXT:gridelements to EXT:container.',
    'category' => 'backend',
    'author' => 'Stefan Bublies',
    'author_email' => 'project@sbublies.de',
    'state' => 'beta',
    'version' => '10.5.1',
    'constraints' => [
        'depends' => [
            'typo3' => '10.4-11.5.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
